package org.loriz.kontakt

import android.content.ContentResolver
import android.content.Context
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.loriz.kontakt.ui.home.model.ContactManager
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ContactManagerTest {

    @Mock
    lateinit var mockContext: Context

    @Mock
    lateinit var mockContentResolver: ContentResolver


    lateinit var contactManager: ContactManager


    val num = "3388432766"
    val prefix = "+39"
    val numWithPrefix = "+393388432766"
    val numWithSpaces = "338 843 2766"



    @Before
    fun setUp() {
        contactManager = ContactManager(mockContext, mockContentResolver)
    }


    
    // normalizeNumber testing
    @Test
    fun givenNumberWithSpaces_whenNormalizing_thenAssureSpacesRemoved() {
        val result = contactManager.normalizeNumber(numWithSpaces)
        Assert.assertEquals(num, result)
    }

    @Test
    fun givenNumberWithSpaces_whenNormalizingAndAddingPrefix_thenAssureSpacesRemovedPrefixAdded() {
        val result = contactManager.normalizeNumber(numWithSpaces, prefix)
        Assert.assertEquals(numWithPrefix, result)
    }

    @Test
    fun givenNormalizedNumber_whenNormalizing_thenAssureNotChanged() {
        val result = contactManager.normalizeNumber(num)
        Assert.assertEquals(num, result)
    }

    @Test
    fun givenNull_whenNormalizing_thenAssureReturnNull() {
        val result = contactManager.normalizeNumber(null)
        val resultWPrefix = contactManager.normalizeNumber(null, prefix)
        Assert.assertEquals(null, result)
        Assert.assertEquals(null, resultWPrefix)
    }



}