package org.loriz.kontakt

import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.loriz.kontakt.ui.detail.model.DetailViewFactory
import org.loriz.kontakt.common.model.Call
import org.loriz.kontakt.common.model.CallType
import org.loriz.kontakt.common.model.PhoneNumber
import org.loriz.kontakt.ui.detail.listener.IDetailInteractionListener
import org.loriz.kontakt.ui.home.view.HomeActivity
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner


@RunWith(RobolectricTestRunner::class)
class DetailViewFactoryTest {

    private lateinit var activity: HomeActivity
    private lateinit var inflater: LayoutInflater

    val factory: DetailViewFactory = DetailViewFactory()

    var listener: IDetailInteractionListener = object : IDetailInteractionListener {
        override fun callTo(number: String) {}
        override fun smsTo(number: String) {}
    }


    //test data
    val phone: PhoneNumber = PhoneNumber("Home", "+39123456789")
    val call: Call = Call("", phone.number, "", CallType.INCOMING, "20 sept")



    @Before
    fun setUp() {
        activity = Robolectric.setupActivity(HomeActivity::class.java)
        inflater = activity.layoutInflater
    }


    //these tests ensure that the functions that generate information for the contact detail are working correctly
    @Test
    fun givenPhoneEntry_whenGeneratingCorrespondingView_assureFieldsAreCorrect() {

        var view = factory.createNumberView(inflater, listener, LinearLayout(activity), phone)

        assertNotNull(view)

        val typeFromView = view.findViewById<TextView>(R.id.contact_detail_entry_number_type)
        val numberFromView = view.findViewById<TextView>(R.id.contact_detail_entry_number)

        assertEquals(phone.type, typeFromView.text)
        assertEquals(phone.number, numberFromView.text)
    }


    @Test
    fun givenCallHistoryEntry_whenGeneratingCorrespondingView_assureFieldsAreCorrect() {

        var view = factory.createHistoryView(inflater, listener, LinearLayout(activity), call)

        assertNotNull(view)

        val typeFromView = view.findViewById<TextView>(R.id.contact_history_entry_number_type)
        val numberFromView = view.findViewById<TextView>(R.id.contact_history_entry_number)
        val timeStampFromView = view.findViewById<TextView>(R.id.contact_history_entry_number_type_date)

        assertEquals("Incoming", typeFromView.text)
        assertEquals(numberFromView.text, call.number)
        assertEquals(timeStampFromView.text, call.timeStamp)

    }
}