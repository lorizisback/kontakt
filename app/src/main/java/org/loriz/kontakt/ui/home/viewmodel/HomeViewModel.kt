package org.loriz.kontakt.ui.home.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.LiveData
import kotlinx.coroutines.experimental.*
import org.loriz.kontakt.common.model.Contact
import org.loriz.kontakt.common.livedata.SingleLiveEvent
import org.loriz.kontakt.ui.detail.view.DetailFragment
import org.loriz.kontakt.ui.home.model.ContactManager


/*
 * This is the ViewModel responsible for fetching the data and handling the actions of the HomeActivity.
 * This extends the AndroidViewModel instead of the basic ViewModel because an application context is needed.
 * Interesting in here: well, the Android Architecture implementation of the MVVM itself and Kotlin Coroutine.
 */
class HomeViewModel(appl: Application) : AndroidViewModel(appl) {

    private var cm: ContactManager = ContactManager(appl.baseContext, appl.baseContext.contentResolver)
    private var contacts: MutableLiveData<ArrayList<Contact>>? = null

    val listItemTapAction = SingleLiveEvent<DetailFragment>()


    //this handles the creation of the DetailFragment for the Activity and fires it on the according SingleLiveEvent
    // (it's just a stripped down MutableLiveData)
    fun navigateToDetail(contact: Contact) {
        listItemTapAction.value = DetailFragment().apply {
            setContact(contact)
        }
    }


    //this is called from the activity to observe the data refresh and get its contacts
    fun getContacts(): LiveData<ArrayList<Contact>>? {
        if (contacts == null) {
            contacts = MutableLiveData<ArrayList<Contact>>()
            refreshContacts()
        }
        return contacts
    }


    //this posts the value of the contact fetch
    private fun refreshContacts() {
        GlobalScope.launch(Dispatchers.Default) {
            contacts?.postValue(cm.getContacts())
        }
    }


}