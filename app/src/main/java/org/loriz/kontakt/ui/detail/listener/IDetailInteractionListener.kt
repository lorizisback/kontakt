package org.loriz.kontakt.ui.detail.listener

interface IDetailInteractionListener {

    fun callTo(number: String)
    fun smsTo(number: String)

}