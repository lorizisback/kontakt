package org.loriz.kontakt.ui.home.listener

import org.loriz.kontakt.common.model.Contact

interface IListItemInteractionListener {
    fun onListItemClick(contact: Contact)
}