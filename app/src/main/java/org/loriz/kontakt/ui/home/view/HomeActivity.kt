package org.loriz.kontakt.ui.home.view

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import kotlinx.android.synthetic.main.view_main.*
import android.widget.Toast
import kotlinx.android.synthetic.main.view_main.home_search_field
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.launch
import org.loriz.kontakt.R
import org.loriz.kontakt.common.model.Contact
import org.loriz.kontakt.ui.home.adapter.ContactListAdapter
import org.loriz.kontakt.ui.home.listener.IListItemInteractionListener
import org.loriz.kontakt.ui.home.model.ContactManager
import org.loriz.kontakt.ui.home.viewmodel.HomeViewModel
import java.util.*
import kotlin.collections.ArrayList

/*
 * This is the main activity. Here the user can access all its phone contacts and, given the correct permission,
 * their call history.
 * Here there are various Android Architectures used, especially the trio ViewModel + LiveData + ListAdapter,
 * Kotlin Coroutines
 * This Activity is part of a MVVM pattern, so it communicates with its ViewModel to interact with the
 * "data layer" (which is just a query w/ ContactResolver) and listens to a livedata
 */
class HomeActivity : AppCompatActivity(), IListItemInteractionListener {

    private val PERMISSIONS_IDENTIFIER = 1337

    private lateinit var contactManager: ContactManager
    private lateinit var adapter: ContactListAdapter
    private lateinit var viewModel: HomeViewModel

    var timer: Timer = Timer()
    val DELAY: Long = 500 //this is the delay between every keystroke and the search of contacts

    private var mContactList: List<Contact>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.view_main)

        //initialization of viewModel and contactmanager,
        //they require dependencies that are not available before activity creation so it's done here
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        contactManager = ContactManager(this, contentResolver)


        //check for permission and handle response
        when (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) + ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG)) {
            PackageManager.PERMISSION_GRANTED -> {
                setUp()
            }
            else -> ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CALL_LOG), PERMISSIONS_IDENTIFIER)
        }

    }


    //just checking if/which permissions are granted and act accordingly
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_IDENTIFIER -> {

                grantResults.takeIf { it.isNotEmpty() }?.let {
                    val readContactsPerms = grantResults[0]
                    val readCallLog = grantResults[1]

                    //this is not a problem, the app just wont query the call history
                    if (readCallLog != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this@HomeActivity, getString(R.string.home_call_log_perms_denied), Toast.LENGTH_LONG).show()
                    }

                    //while this will not crash the app or anything, you cant have a contacts app
                    //and expect to find contacts in it without granting permission
                    if (readContactsPerms == PackageManager.PERMISSION_GRANTED) {
                        setUp()
                    } else {
                        Toast.makeText(this@HomeActivity, getString(R.string.home_contact_perms_denied), Toast.LENGTH_LONG).show()
                        finish()
                    }

                    return
                }

                //if no permissions are given, just get out
                Toast.makeText(this@HomeActivity, getString(R.string.home_no_permission_granted_error), Toast.LENGTH_LONG).show()
                finish()

                return
            }
        }
    }


    //just for style, keep all the initializing methods in one place to call them easily
    private fun setUp() {
        prepareUI()
        refreshContacts()
        observeItemInteraction()
    }


    private fun prepareUI() {
        home_recyclerview.layoutManager = LinearLayoutManager(this)
        adapter = ContactListAdapter(this, this)
        home_recyclerview.adapter = adapter
        home_search_field.addTextChangedListener(object : TextWatcher {
            //these two methods are part of the textwatcher and must be implemented even if they are not needed...
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            //this method uses a timer to avoid searching for a specific contact on every keystroke which is
            //bad for performance (even if it's done in a coroutine). If the timer runs out before a new keystroke cancels
            //it, the word is searched and the results displayed
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                timer.cancel()
                timer = Timer()
                timer.schedule(
                        object : TimerTask() {
                            override fun run() {
                                if (s.isNotBlank()) {
                                    //I love coroutines, no seriously I do
                                    GlobalScope.launch(Dispatchers.Default) {
                                        updateView(mContactList?.filter { it.name.contains(s, true) })

                                    }
                                } else {
                                    resetSearch()
                                }
                            }
                        },
                        DELAY
                )
            }


        })
    }


    //this proxies the user interaction with one item to the viewmodel to handle the action as mvvm pattern specifications
    private fun observeItemInteraction() {
        viewModel.listItemTapAction.observe(this, Observer {
            it?.show(supportFragmentManager, "Contact Detail")
        })
    }


    //observe the viewmodel's livedata to know when the list of contacts is ready to be submitted to the Adapter
    fun refreshContacts() {
        viewModel.getContacts()?.observe(this, Observer<ArrayList<Contact>> { t ->
            onContactsRefreshSuccess(t)
        })
        statusProgressBar(true)
    }


    //this just shows the original list fetched by the viewmodel
    private fun resetSearch() = updateView(mContactList)


    //this methods submits the contact list to the ListAdapter (which wraps the old RecyclerView.Adapter)
    //the first time that this gets called it saves the original list, so it can be used for searching purposes
    private fun updateView(contacts: List<Contact>?) {
        if (mContactList == null) {
            mContactList = contacts
        }
        adapter.submitList(contacts)
    }


    //pretty self explanatory, it just hides/shows the progressbar thus enabling/disabling interaction
    private fun statusProgressBar(status: Boolean) {
        var visibility = if (status) View.VISIBLE else View.GONE
        home_progress_layout.visibility = visibility
    }


    // if the refresh was successful, proceed and submit the contact list to the adapter
    fun onContactsRefreshSuccess(contacts: ArrayList<Contact>?) {
        if (contacts != null && contacts.isNotEmpty()) {
            updateView(contacts)
            statusProgressBar(false)
        } else {
            onContactsRefreshFailure()
        }
    }


    // something went wrong when fetching the contacts, inform the user
    fun onContactsRefreshFailure() {
        Toast.makeText(this, getString(R.string.home_fetching_failed), Toast.LENGTH_SHORT).show()
        statusProgressBar(false)
    }


    //on click of list item navigate to its corresponding detail using the viewmodel (pretty much a proxy for the interaction)
    override fun onListItemClick(contact: Contact) {
        viewModel.navigateToDetail(contact)
    }


    //override base onBackPressed method to reset the view if searching or exiting if not
    override fun onBackPressed() {
        if (!home_search_field.text.isNullOrBlank()) {
            home_search_field.setText("")
            resetSearch()
        } else {
            super.onBackPressed()
        }
    }


}