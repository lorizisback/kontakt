package org.loriz.kontakt.ui.home.adapter

import android.content.Context
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mikhaellopez.circularimageview.CircularImageView
import org.loriz.kontakt.R
import org.loriz.kontakt.common.model.Contact
import org.loriz.kontakt.ui.home.listener.IListItemInteractionListener

/*
 * This is the ListAdapter, which is just a new wrapper for the RecyclerView made to simplify
 * interactions with the MVVM/LiveData approach.
 * It follows some DIFF rules defined in the Contact to know how to update the list in case of a
 * new dataset from the LiveData.
 */
class ContactListAdapter(val context: Context, val listener: IListItemInteractionListener) : ListAdapter<Contact, ContactListAdapter.CLViewHolder>(Contact.DIFF_CALLBACK) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CLViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.holder_contact, parent, false)

        return CLViewHolder(view)

    }

    override fun onBindViewHolder(holder: CLViewHolder, position: Int) {

        val contact = getItem(position)

        holder.apply {

            if (contact.image != null) {
                image.setImageBitmap(contact.image)
            } else {
                image.setImageResource(R.drawable.contact_icon)
                image.setBackgroundColor(contact.associatedColor)
            }

            mainText.text = contact.name
            subText.text = when (contact.phoneNumbers.size) {
                1 -> contact.phoneNumbers[0].number
                else -> context.getString(R.string.contact_multiple_numbers)
            }

            //this is needed to respect the mvvm pattern
            container.setOnClickListener {
                listener.onListItemClick(contact)
            }

        }




    }

    inner class CLViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var container: View = itemView.findViewById(R.id.holder_container)
        var image: CircularImageView = itemView.findViewById(R.id.holder_image)
        var mainText: TextView = itemView.findViewById(R.id.holder_text)
        var subText: TextView = itemView.findViewById(R.id.holder_subtext)
    }

}