package org.loriz.kontakt.ui.detail.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.View
import android.support.v4.widget.NestedScrollView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import com.github.florent37.kotlin.pleaseanimate.please
import com.mikhaellopez.circularimageview.CircularImageView
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.android.Main
import kotlinx.coroutines.experimental.launch
import org.loriz.kontakt.R
import org.loriz.kontakt.common.model.Contact
import org.loriz.kontakt.ui.detail.viewmodel.DetailViewModel


/*
 * This Fragment exposes to the users various details regarding the selected contact in a bottom sheet dialog.
 * It uses Android Architecture's LiveData, ViewModel and Kotlin Coroutines to make the job easier
 * and safer.
 * It also uses KotlinPleaseAnimate to animate the header on scroll for aesthetics
 */
class DetailFragment : BottomSheetDialogFragment() {

    private lateinit var contact: Contact
    private lateinit var viewModel: DetailViewModel


    //fragment should never receive an object from its constructor so it's passed here
    fun setContact(contact: Contact) {
        this.contact = contact
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DetailViewModel::class.java)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_detail, container, false)

        val headerTop = view.findViewById<View>(R.id.detail_header_top)
        val headerBottom = view.findViewById<View>(R.id.detail_header_bottom)
        val image = view.findViewById<CircularImageView>(R.id.detail_image)
        val name = view.findViewById<TextView>(R.id.detail_name)
        val itemsScrollView = view.findViewById<NestedScrollView>(R.id.fragment_detail_list)
        val numberContainer = view.findViewById<LinearLayout>(R.id.numbers_container)
        val callContainer = view.findViewById<LinearLayout>(R.id.call_container)

        //prepare animation for header using KotlinPleaseAnimate
        val headerAnim = please {
            animate(image) toBe {
                leftOfItsParent(20f)
                topOfItsParent(20f)
                scale(0.5f, 0.5f)
            }

            animate(name) toBe {
                rightOf(image, 15f)
                sameCenterVerticalAs(image)
            }

            animate(headerBottom) toBe {
                belowOf(headerTop)
                height(70, toDp = true)
            }

        }

        //bind animation to the scroll of the items
        itemsScrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, _ ->
            val percent = scrollY * 1f / v.maxScrollAmount
            headerAnim.setPercent(percent)
        })

        //customize view to match user color
        headerTop.background.setColorFilter(contact.associatedColor, PorterDuff.Mode.SRC_IN)
        headerBottom.setBackgroundColor(contact.associatedColor)

        //set user image or stock image
        if (contact.image != null) {
            image.setImageBitmap(contact.image)
        } else {
            image.setImageResource(R.drawable.contact_icon)
            image.setBackgroundColor(contact.associatedColor)
        }

        name.text = contact.name

        //enable the observers to react to the viewmodel's livedata changes to update the ui with the generated views
        viewModel.preparedPhoneViews.observe(this, Observer {
            numberContainer.addView(it)
            numberContainer.addView(inflater.inflate(R.layout.divider, numberContainer, false))
        })

        viewModel.preparedHistoryViews.observe(this, Observer {
            callContainer.addView(it)
        })

        //use this Kotlin Coroutine to enqueue on the viewmodel the asyncronous generation of the details of the contact
        //using the Main Dispatcher (which is Android-only) it's possible to interact freely with the UI
        GlobalScope.launch(Dispatchers.Main) {

            contact.phoneNumbers.forEach {
                viewModel.preparePhoneView(inflater, numberContainer, it)
            }

            contact.associatedCalls.takeIf { it.isNotEmpty() }?.let {
                callContainer.addView(inflater.inflate(R.layout.fragment_detail_history_header, callContainer, false).apply { setBackgroundColor(contact.associatedColor) })
                it.forEach {
                    viewModel.prepareHistoryView(inflater, callContainer, it)
                }
            }

        }

        return view
    }


}