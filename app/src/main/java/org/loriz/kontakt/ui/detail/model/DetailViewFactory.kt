package org.loriz.kontakt.ui.detail.model

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.loriz.kontakt.R
import org.loriz.kontakt.common.model.Call
import org.loriz.kontakt.common.model.CallType
import org.loriz.kontakt.common.model.PhoneNumber
import org.loriz.kontakt.ui.detail.listener.IDetailInteractionListener


/*
 * This Factory creates the view and customizes them based on the kind of method and input. This is to be
 * used from the ViewModel of the DetailFragment
 */
class DetailViewFactory {


    //this method creates a view which represents a availab;e number for a contact
    fun createNumberView(inflater: LayoutInflater, listener: IDetailInteractionListener, root: ViewGroup?, phone: PhoneNumber): View {
        val view = inflater.inflate(R.layout.fragment_detail_entry, root, false)

        val typeField = view.findViewById<TextView>(R.id.contact_detail_entry_number_type)
        val numberField = view.findViewById<TextView>(R.id.contact_detail_entry_number)
        val phoneBtn = view.findViewById<ImageView>(R.id.contact_detail_entry_phone)
        val smsBtn = view.findViewById<ImageView>(R.id.contact_detail_entry_sms)


        typeField.text = phone.type
        numberField.text = phone.number

        phoneBtn.setOnClickListener {
            listener.callTo(phone.number)
        }

        smsBtn.setOnClickListener {
            listener.smsTo(phone.number)
        }

        return view
    }



    //this method creates a view which represents an history entry of a call for a contact
    fun createHistoryView(inflater: LayoutInflater, listener: IDetailInteractionListener, root: ViewGroup?, callDetails: Call): View {

        val view = inflater.inflate(R.layout.fragment_detail_history_entry, root, false)

        val numberField = view.findViewById<TextView>(R.id.contact_history_entry_number)
        val typeField = view.findViewById<TextView>(R.id.contact_history_entry_number_type)
        val dateField = view.findViewById<TextView>(R.id.contact_history_entry_number_type_date)

        numberField.setText(callDetails.number)
        typeField.apply {

            when (callDetails.callType) {
                CallType.OUTGOING -> {
                    text = root?.context?.getString(R.string.call_type_outgoing)
                    setTextColor(Color.GREEN)
                }

                CallType.INCOMING -> {
                    text = root?.context?.getString(R.string.call_type_incoming)
                    setTextColor(Color.BLUE)
                }

                CallType.MISSED -> {
                    text = root?.context?.getString(R.string.call_type_missed)
                    setTextColor(Color.RED)
                }

                CallType.OTHER -> {
                    text = root?.context?.getString(R.string.call_type_other)
                    setTextColor(Color.YELLOW)
                }
            }
        }

        dateField.text = callDetails.timeStamp

        return view

    }

}