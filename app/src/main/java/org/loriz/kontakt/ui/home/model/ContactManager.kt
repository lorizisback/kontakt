package org.loriz.kontakt.ui.home.model

import android.Manifest
import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.CallLog
import android.provider.ContactsContract
import android.provider.ContactsContract.CommonDataKinds.Phone
import android.support.v4.content.ContextCompat
import android.support.v4.content.PermissionChecker
import org.loriz.kontakt.R
import org.loriz.kontakt.common.model.*
import org.loriz.kontakt.common.util.Palette
import java.lang.Long
import java.text.DateFormat
import java.util.*
import kotlin.collections.ArrayList

/*
 * This is the ContactManager. Here the contacts are fetched from the system using a ContentResolver,
 * this is just a system wrapper for interacting with the contacts.db
 * I just wanted to find a way to use Android Room for the DB but for security reasons the contact database
 * is only accessible using the given ContentResolver (obliviously)...
 * Replicating it in a new Room db just for the sake of using it would have been a bad idea performance-wise,
 * ux-wise, needed a service running and checking constantly if the Room db needed an update...
 * Pretty much a bad idea in general.
 *
 *
 */
class ContactManager(private val context: Context, private val contentResolver: ContentResolver) {

    //this queries the system Contacts db using the contentResolver. It gives a cursor that must be used
    //to navigate the result of the query. Here the contacts and (if permission is granted) part of the call history
    //are fetched to be used in the rest of the app.
    fun getContacts(): ArrayList<Contact> {

        val list = arrayListOf<Contact>()
        val callList = arrayListOf<Call>()

        val contentResolver = contentResolver
        val df = DateFormat.getDateTimeInstance()

        contentResolver.let {

            //if the permissions for the Call log are granted, fetch that data first
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) == PermissionChecker.PERMISSION_GRANTED) {

                val sortOrder = String.format("%s limit 200 ", CallLog.Calls.DATE + " DESC")
                val callsCursor = contentResolver.query(CallLog.Calls.CONTENT_URI, null, null, null, sortOrder)

                callsCursor?.let {
                    if (callsCursor.count > 0) {

                        val numberIndex = callsCursor.getColumnIndex(CallLog.Calls.NUMBER)
                        val durationIndex = callsCursor.getColumnIndex(CallLog.Calls.DURATION)
                        val dateIndex = callsCursor.getColumnIndex(CallLog.Calls.DATE)
                        val typeIndex = callsCursor.getColumnIndex(CallLog.Calls.TYPE)

                        if (callsCursor.moveToFirst()) {

                            while (callsCursor.moveToNext()) {

                                val type = callsCursor.getString(typeIndex).toInt()

                                val callType = when (type) {
                                    CallLog.Calls.INCOMING_TYPE -> CallType.INCOMING
                                    CallLog.Calls.OUTGOING_TYPE -> CallType.OUTGOING
                                    CallLog.Calls.MISSED_TYPE -> CallType.MISSED
                                    else -> CallType.OTHER
                                }

                                //normalize and put into an array each result found
                                val phNumber = normalizeNumber(callsCursor.getString(numberIndex), "+39")

                                phNumber?.let {
                                    val callDate = callsCursor.getString(dateIndex)
                                    val callDayTime = df.format(Date(Long.valueOf(callDate)))
                                    val callDuration = callsCursor.getString(durationIndex)

                                    callList.add(Call("",
                                            phNumber,
                                            callDuration,
                                            callType,
                                            callDayTime)
                                    )
                                }


                            }

                            callsCursor.close()
                        }
                    }
                }
            }


            //now fetches the contacts
            val cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)

            cursor?.let {

                if (cursor.count > 0) {
                    while (cursor.moveToNext()) {

                        val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))

                        //if this user has a phone number, keep it and keep querying
                        if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {

                            val cursorInfo = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", arrayOf(id), null)

                            val inputStream = ContactsContract.Contacts.openContactPhotoInputStream(contentResolver,
                                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id.toLong()))

                            var photo: Bitmap? = null
                            if (inputStream != null) {
                                photo = BitmapFactory.decodeStream(inputStream)
                            }

                            while (cursorInfo!!.moveToNext()) {
                                var found = false

                                val number = normalizeNumber(cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)), "+39")
                                val type = cursorInfo.getInt(cursorInfo.getColumnIndex(Phone.TYPE))

                                //one i get the number, cross reference the calls array to add the calls list to the Contact item
                                number?.let {

                                    //keep only the Call items matching the actual number
                                    val callDetail = callList.filter { it.number == number }

                                    //if Contact was already added, just add this Call info as needed
                                    list.find { it.id == id }?.apply {
                                        found = true

                                        if (phoneNumbers.none { it.number == number }) {
                                            this.phoneNumbers.add(PhoneNumber(resolveType(type), number))

                                            this.associatedCalls.addAll(callDetail)
                                        }


                                    }

                                    //if the Contact is new and must be added, add it with the Call data already
                                    if (!found) {
                                        list.add(
                                                Contact(id,
                                                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)),
                                                        arrayListOf(PhoneNumber(resolveType(type), it)),
                                                        photo,
                                                        Palette.getRandColor(),
                                                        callDetail as ArrayList<Call>))
                                    }
                                }
                            }
                            cursorInfo.close()
                        }
                    }
                    cursor.close()
                }
            }

            //sort the result by name
            if (list.isNotEmpty()) {
                list.sortBy { it.name }
            }

        }

        return list
    }


    //matches the type used from the system with a prettified string
    fun resolveType(type: Int): String {
        return when (type) {
            Phone.TYPE_HOME -> context.getString(R.string.contact_type_home)
            Phone.TYPE_MOBILE -> context.getString(R.string.contact_type_mobile)
            Phone.TYPE_WORK -> context.getString(R.string.contact_type_work)
            Phone.TYPE_FAX_HOME -> context.getString(R.string.contact_type_home_fax)
            Phone.TYPE_FAX_WORK -> context.getString(R.string.contact_type_work_fax)
            Phone.TYPE_MAIN -> context.getString(R.string.contact_type_main)
            Phone.TYPE_OTHER -> context.getString(R.string.contact_type_other)
            Phone.TYPE_CUSTOM -> context.getString(R.string.contact_type_custom)
            Phone.TYPE_PAGER -> context.getString(R.string.contact_type_pager)
            else -> context.getString(R.string.contact_type_unknown)
        }
    }


    //remove spaces from number and add a prefix if desired
    fun normalizeNumber(number: String?, countryPrefix: String? = null): String? {
        number?.let {
            val re = Regex("[^+0-9]")
            val num = re.replace(number, "")
            return if (!num.startsWith("+") && !countryPrefix.isNullOrBlank()) {
                countryPrefix + num
            } else {
                num
            }
        }
        return null
    }

}