package org.loriz.kontakt.common.util

import android.graphics.Color
import java.util.*


//statically available palette of colors to use for contacts, nothing much
enum class Palette(val color: Int) {
    PEACH(Color.parseColor("#f2aeae")),
    FUCHSIA(Color.parseColor("#f6a5c0")),
    PURPLE(Color.parseColor("#d7a8df")),
    VIOLET(Color.parseColor("#c2b0e2")),
    BLUE(Color.parseColor("#b2b9e1")),
    LIGHT_BLUE(Color.parseColor("#a6d4fa")),
    SKY_BLUE(Color.parseColor("#9adcfb")),
    CYAN(Color.parseColor("#99e4ee")),
    EMERALD(Color.parseColor("#99d5cf")),
    GREEN(Color.parseColor("#b7deb8")),
    LIME(Color.parseColor("#d0e7b7")),
    LIGHT_GREEN(Color.parseColor("#ebf1af")),
    YELLOW(Color.parseColor("#fff7b0")),
    AMBER(Color.parseColor("#ffe69b")),
    MELON(Color.parseColor("#ffd699")),
    SALMON(Color.parseColor("#ffbba7"));

    companion object {

        private val RANDOM = Random()

        fun getRandColor() : Int {
            return values()[RANDOM.nextInt(values().size)].color
        }

    }
}