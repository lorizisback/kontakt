package org.loriz.kontakt.common.model

import android.graphics.Bitmap
import android.support.v7.util.DiffUtil


data class Contact(
        var id: String,
        var name: String,
        var phoneNumbers: ArrayList<PhoneNumber>,
        var image: Bitmap?,
        var associatedColor: Int,
        var associatedCalls: ArrayList<Call>) {

    companion object {

        //this diff callback is used in the ListAdapter to update its views in an efficient way when the dataset changes
        val DIFF_CALLBACK: DiffUtil.ItemCallback<Contact> = object : DiffUtil.ItemCallback<Contact>() {

            override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
                return (oldItem.id == newItem.id)
            }

            override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
                return (oldItem.name == newItem.name &&
                        oldItem.phoneNumbers == newItem.phoneNumbers)
            }

        }

    }

}