package org.loriz.kontakt.common.model

data class PhoneNumber(val type: String, val number: String)