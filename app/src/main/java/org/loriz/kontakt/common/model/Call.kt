package org.loriz.kontakt.common.model


data class Call(
        var userId: String,
        var number: String,
        var duration: String,
        var callType: CallType,
        var timeStamp: String)


enum class CallType {
    INCOMING, OUTGOING, MISSED, OTHER
}
